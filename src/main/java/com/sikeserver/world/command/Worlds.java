package com.sikeserver.world.command;

import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.world.WorldManager;

public class Worlds implements CommandExecutor {
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(WorldManager.getPlugin().core.getMessage("Error.Common.Console"));
			return true;
		}
		
		List<String> messages = WorldManager.getPlugin().core.getMessageList("Message.World.Worlds");
		for (String msg : messages) {
			((Player)sender).sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
		}
		return true;
	}
}
