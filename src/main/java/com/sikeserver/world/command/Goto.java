package com.sikeserver.world.command;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sikeserver.core.CoreManager;
import com.sikeserver.world.WorldManager;

public class Goto implements CommandExecutor {
	private CoreManager core;
	private WorldManager plugin;
	
	public Goto() {
		plugin = WorldManager.getPlugin();
		core = plugin.core;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage(core.getMessage("Error.Common.Console"));
			return true;
		}
		
		Player p = (Player)sender;
		if (label.equalsIgnoreCase("goto")) {
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("Main_nether") || args[0].equalsIgnoreCase("Main_the_end"))
					return false;
				
				World w = Bukkit.getWorld(args[0]);
				if (w == null) {
					sender.sendMessage(core.getMessage("Error.World.NotFound"));
					return false;
				}
				
				p.teleport(w.getSpawnLocation());
				return true;
			} else if (args.length > 1) {
				sender.sendMessage(core.getMessage("Error.Common.Invalid"));
				return false;
			}
		}
		
		return false;
	}
}
