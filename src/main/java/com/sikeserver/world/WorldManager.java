package com.sikeserver.world;

import java.util.List;

import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.plugin.java.JavaPlugin;

import com.sikeserver.core.CoreManager;
import com.sikeserver.core.util.ConfigManager;
import com.sikeserver.world.command.Goto;
import com.sikeserver.world.command.Worlds;

public class WorldManager extends JavaPlugin {
	public CoreManager core;
	private static WorldManager plugin;
	
	private ConfigManager worldList;
	public List<World> worlds;
	
	@Override
	public void onEnable() {
		plugin = this;
		core = CoreManager.getPlugin();
		
		try {
			worldList = new ConfigManager("worlds.yml", core);
		} catch (Exception e) {
			getLogger().warning("Error occured while loading config file!");
			e.printStackTrace();
		}
		
		getCommand("goto").setExecutor(new Goto());
		getCommand("worlds").setExecutor(new Worlds());
		
		List<String> worldNames = worldList.getStringList("Worlds");
		for(String worldName : worldNames) {
			new WorldCreator(worldName).createWorld();
		}
	}
	
	public static WorldManager getPlugin() {
		return plugin;
	}
}
